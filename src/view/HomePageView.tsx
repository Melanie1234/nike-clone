import Link from 'next/link'
import React from 'react'
import Header from '../component/Header'

function HomePageView() {
    return (
        <div
            className='w-screen min-h-screen flex flex-col flex-1 '
        >
            <div
                className='w-full px-12 py-2 bg-black bg-opacity-5 flex justify-between items-center'
            >
                <div
                    className='flex flex-row space-x-3'
                >
                    <div
                        className='w-7 h-7 bg-black rounded-full'
                    >

                    </div>

                    <div
                        className='w-7 h-7 bg-black rounded-full'
                    >

                    </div>
                </div>

                <nav
                    className=''
                >
                    <ul
                        className='flex space-x-3 divide-x-2 divide-black items-center'
                    >
                        <li
                            className='pl-3 text-xs font-medium'
                        >
                            <Link
                                href={'/'}
                                className='hover:text-[#757575] hover:underline'
                            >
                                <span>
                                    {`Trouver un magasin`}
                                </span>
                            </Link>
                        </li>
                        <li
                            className='pl-3 text-xs font-medium'
                        >
                            <Link
                                href={'/'}
                                className='hover:text-[#757575] hover:underline'
                            >
                                <span>
                                    {`Aide`}
                                </span>
                            </Link>
                        </li>
                        <li
                            className='pl-3 text-xs font-medium'
                        >
                            <Link
                                href={'/'}
                                className='hover:text-[#757575] hover:underline'
                            >
                                <span>
                                    {`Nous rejoindre`}
                                </span>
                            </Link>
                        </li>
                        <li
                            className='pl-3 text-xs font-medium'
                        >
                            <Link
                                href={'/'}
                                className='hover:text-[#757575] hover:underline'
                            >
                                <span>
                                    {`S'identifier`}
                                </span>
                            </Link>
                        </li>
                    </ul>

                </nav>
            </div>

            <Header/>

            <div>
                        <img src="https://cdn.discordapp.com/attachments/1193839075682156588/1193854774202880000/nike-just-do-it.jpg?ex=65ae3ae6&is=659bc5e6&hm=9f707dacf6f0168763cbf0541c8485f8c7cb9fe278ba72901bbb094826cc4df9&" alt="" />
                    </div>
        </div>
    )
}


export default HomePageView
